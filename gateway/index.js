const express = require("express");
const http = require("http");

const app = express();

const server = http.createServer(app);

const { Server } = require("socket.io");
const io = new Server(server);

const api = require("../api");

server.listen(80, () => {
  console.log("server inicialize");
  io.on("connection", (socket) => {
    console.log("new conection", socket.id);

    socket.on("req:microservice:view", async ({}) => {
      try {
        console.log("req:microservice:view");
        const { statusCode, data, message } = await api.View({});
        return io
          .to(socket.id)
          .emit("res:microservice:view", { statusCode, data, message });
      } catch (error) {
        console.log(error);
      }
    });
    socket.on("req:microservice:create", async ({ age, color, name }) => {
      try {
        console.log("req:microservice:create", { age, color, name });
        const { statusCode, data, message } = await api.create({});
        return io
          .to(socket.id)
          .emit("res:microservice:create", { statusCode, data, message });
      } catch (error) {
        console.log(error);
      }
    });
    socket.on("req:microservice:delete", async ({ id }) => {
      try {
        console.log("req:microservice:delete", { id });
        const { statusCode, data, message } = await api.Delete({});
        return io
          .to(socket.id)
          .emit("res:microservice:delete", { statusCode, data, message });
      } catch (error) {
        console.log(error);
      }
    });
    socket.on("req:microservice:update", async ({ name, color, age, id }) => {
      try {
        console.log("req:microservice:update", { id });
        const { statusCode, data, message } = await api.update({});
        return io
          .to(socket.id)
          .emit("res:microservice:update", { statusCode, data, message });
      } catch (error) {
        console.log(error);
      }
    });
    socket.on("req:microservice:FinONe", async ({ id }) => {
      try {
        console.log("req:microservice:FinONe", { id });
        const { statusCode, data, message } = await api.FinOne({ id });
        return io
          .to(socket.id)
          .emit("res:microservice:FinONe", { statusCode, data, message });
      } catch (error) {
        console.log(error);
      }
    });
  });
});
