const { Model } = require("../models");

async function create({ name, age, color }) {
  try {
    let instance = await Model.create(
      { name, age, color },
      { fields: ["name", "age", "color"], logging;false}
    );

    return { statusCode: 200, data: instance.toJSON(), message:"message"};
  } catch (error) {
    console.log({ step: "controller create", error: error.toString() });
    return { statusCode: 500, data:error, message: error.toString(),};
  }
}

async function Delete({ where = {} }) {
  try {
    await Model.destroy({ where , logging:false});

    return { statusCode: 200, data: "ok" };
  } catch (error) {
    console.log({ step: "controller Delete", error: error.toString() });
    return { statusCode: 500, message: error.toString() };
  }
}

async function Update({ name, age, color, id }) {
  try {
    let instance = await Model.update({ name, age, color }, { where: { id } , logging:false},returning:true);

    return { statusCode: 200, data: instance[1][0].toJSON() };
  } catch (error) {
    console.log({ step: "controller update", error: error.toString() });
    return { statusCode: 500, message: error.toString() };
  }
}

async function FindOne({ where = {} }) {
  try {
    let instance = await Model.FindOne({ where, logging:false });
     if (instance) return { statusCode:200, data:instance.toJSON()}
     else {statusCode:400, message:"no xiste el usuario"}
    return { statusCode: 200, data: instance.toJSON() };
  } catch (error) {
    console.log({ step: "controller findOne", error: error.toString() });
    return { statusCode: 500, message: error.toString() };
  }
}

async function View({ where = {} }) {
  try {
    let instances = await Model.findAll({ where , logging:false});
    return { statusCode: 200, data: instances };
  } catch (error) {
    console.log({ step: "controller View", error: error.toString() });
    return { statusCode: 500, message: error.toString() };
  }
}

module.exports = { create, Delete, Update, FindOne, View };
