const Controllers = require("../Controllers");

async function create({ name, edad, color }) {
  try {
    let { statusCode, data, message } = await Controllers.create({
      name,
      edad,
      color,
    });
    return { statusCode, data, message };
  } catch (error) {
    console.log({ step: "service create", error: error.toString() });
    return { statusCode: 500, message: error.toString() };
  }
}

async function Delete({ id }) {
  try {
    const findOne = await Controllers.findOne({where:{id}})
    if (findOne.statusCode!==200){
      let response = {
        400:{statusCode:400, message:"no existe el usuario a eliminar"},
        500: {statusCode:400, message: InternalError}
      }
      return response [findOne.statusCode]
    }
    
    let { statusCode, data, message } = const del = await Controllers.Delete({
      where: { id },
    });
    if (del.statusCode===209) return {statusCode:200, data: findOne.data}
    return {statusCode:400, message:InternalError}
    return { statusCode, data, message };
  } catch (error) {
    console.log({ step: "service Delete", error: error.toString() });
    return { statusCode: 500, message: error.toString() };
  }
}

async function update({ name, edad, color, id }) {
  try {
    let { statusCode, data, message } = await Controllers.update({
      name,
      edad,
      color,
      id,
    });
    return { statusCode, data, message };
  } catch (error) {
    console.log({ step: "service update", error: error.toString() });
    return { statusCode: 500, message: error.toString() };
  }
}

async function findOne({ id }) {
  try {
    let { statusCode, data, message } = await Controllers.findOne({
      where: { id },
    });
    return { statusCode, data, message };
  } catch (error) {
    console.log({ step: "service findOne", error: error.toString() });
    return { statusCode: 500, message: error.toString() };
  }
}

async function view({}) {
  try {
    let { statusCode, data, message } = await Controllers.View({});
    return { statusCode, data, message };
  } catch (error) {
    console.log({ step: "service view", error: error.toString() });
    return { statusCode: 500, message: error.toString() };
  }
}

module.exports = { create, Delete, update, findOne, view };
