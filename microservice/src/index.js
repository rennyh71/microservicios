const { SyncDB } = require("./models");
const { run } = require("./adapters/processor");

module.exports = { SyncDB, run };
