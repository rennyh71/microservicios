const { services } = require("../services");
const { internalError } = require("../settings");
const {
  queueCreate,
  queueDelete,
  queueUpdate,
  queueFinOne,
  queueView,
} = require("./index");

async function Create(job, done) {
  try {
    const { name, age, color } = job.data;
    let { statusCode, data, message } = await services.create({
      name,
      age,
      color,
    });
    done(null, { statusCode, data, message });
  } catch (error) {
    console.log({ step: "adapter queueCreate", error: error.toString() });
    done(null, { statusCode, data, message: internalError });
  }
}
async function Delete(job, done) {
  try {
    const { id } = job.data;
    let { statusCode, data, message } = await services.update({ id });
    done(null, { statusCode, data, message });
  } catch (error) {
    console.log({ step: "adapter queueDelete", error: error.toString() });
    done(null, { statusCode, data, message: internalError });
  }
}
async function Update(job, done) {
  try {
    const { name, age, color, id } = job.data;
    let { statusCode, data, message } = await services.update({
      name,
      edad,
      color,
      id,
    });
    done(null, { statusCode, data, message });
  } catch (error) {
    console.log({ step: "adapter queueUpdate", error: error.toString() });
    done(null, { statusCode, data, message: internalError });
  }
}
async function FinOne(job, done) {
  try {
    const { id } = job.data;
    let { statusCode, data, message } = await services.finOne({ id });
    done(null, { statusCode, data, message });
  } catch (error) {
    console.log({ step: "adapter queueFinOne", error: error.toString() });
    done(null, { statusCode, data, message: internalError });
  }
}

async function view(job, done) {
  try {
    const {} = job.data;
    console.log(job.id);
    const { statusCode, data, message } = await services.view({});
    done(null, { statusCode, data, message });
  } catch (error) {
    console.log({ step: "adapter queueView", error: error.toString() });
    done(null, { statusCode: 500, message: internalError });
  }
}

async function run() {
  try {
    console.log("vamos a inicializar worker");
    queueView.process(view);
    queueCreate.process(Create);
    queueDelete.process(Delete);
    queueUpdate.process(Update);
    queueFinOne.process(FinOne);
  } catch (error) {
    console.log(error);
  }
}

module.exports = { view, Create, Delete, Update, FinOne, run };
