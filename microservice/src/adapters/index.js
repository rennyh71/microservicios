const bull = require("bull");
const { redis } = require("../settings");

const opcions = { host: redis.host, port: redis.port };

const queueCreate = bull("curso:create", opcions);
const queueDelete = bull("curso:Delete", opcions);
const queueUpdate = bull("curso:update", opcions);
const queueFinOne = bull("curso:finOne", opcions);
const queueView = bull("curso:view", opcions);

module.exports = {
  queueCreate,
  queueDelete,
  queueUpdate,
  queueFinOne,
  queueView,
};
