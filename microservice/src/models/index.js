const { DataTypes } = require("sequelize");
const { sequelize } = require("../settings");

const Model = sequelize.define("curso", {
  name: { type: DataTypes.STRING },
  age: { type: DataTypes.BIGINT },
  color: { type: DataTypes.STRING },
});

async function SyncDB() {
  try {
    console.log("vamos a inicializar base de datos");
    const a = await Model.sync({ logging: false, force: true });
    console.log(a);
    console.log(" base de datos inicializada");
    return { statusCode: 200, data: "ok" };
  } catch (error) {
    console.log(error);
    return { statusCode: 500, message: error.toString() };
  }
}

module.exports = { SyncDB, Model };
