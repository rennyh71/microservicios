const dotenv = require("dotenv");
const { Sequelize } = require("sequelize");
dotenv.config();

const redis = {
  host: process.env.REDIS_HOST,
  port: process.env.REDIS_PORT,
};

const internalError = "no podemos procesar su informacion en estos momentos";

// const db = {
//   host: process.env.POSTGRES_HOST,
//   port: process.env.POSTGRES_PORT,
//   database: process.env.POSTGRES_DB,
//   username: process.env.POSTGRES_USER,
//   password: process.env.POSTGRES_PASSWORD,
// };
// const sequelize = new {
//   host: db.host,
//   port: db.port,
//   database: db.database,
//   username: db.username,
//   password: db.password,
//   dialect: "postgres",
// }();

const sequelize = new Sequelize({
  host: process.env.POSTGRES_HOST,
  port: process.env.POSTGRES_PORT,
  database: process.env.POSTGRES_DB,
  username: process.env.POSTGRES_USER,
  password: process.env.POSTGRES_PASSWORD,
  dialect: "postgres",
});

module.exports = { redis, internalError, sequelize };
