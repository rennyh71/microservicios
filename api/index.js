const bull = require("bull");
const { redis } = require("../microservice/src/settings");

const redis = { host: "192.168.1.27", port: 6376 };

const redis = {
  host: process.env.REDIS_HOST,
  port: process.env.REDIS_PORT,
};

const opcions = { host: redis.host, port: redis.port };

const queueCreate = bull("curso:create", opcions);
const queueDelete = bull("curso:Delete", opcions);
const queueUpdate = bull("curso:update", opcions);
const queueFinOne = bull("curso:finOne", opcions);
const queueView = bull("curso:view", opcions);

async function create({ name, age, color }) {
  try {
    const job = await queueCreate.add({ name, age, color });
    const { statusCode, data, message } = await job.finished();
    return { statusCode, data, message };
  } catch (error) {
    console.log(error);
  }
}
async function Delete({ id }) {
  try {
    const job = await queueDelete.add({ id });
    const { statusCode, data, message } = await job.finished();
    return { statusCode, data, message };
  } catch (error) {
    console.log(error);
  }
}
async function update({ name, color, age, id }) {
  try {
    const job = await queueUpdate.add({ name, color, age, id });
    const { statusCode, data, message } = await job.finished();
    return { statusCode, data, message };
  } catch (error) {
    console.log(error);
  }
}
async function FinOne({ id }) {
  try {
    const job = await queueFinOne.add({ name });
    const { statusCode, data, message } = await job.finished();
    return { statusCode, data, message };
  } catch (error) {
    console.log(error);
  }
}
async function View({}) {
  try {
    console.log("ingreso solicitud view");
    const job = await queueView.add({});
    const { statusCode, data, message } = await job.finished();
    console.log({ statusCode, data, message });
    return { statusCode, data, message };
  } catch (error) {
    console.log(error);
  }
}

async function main() {
  await create({ name: "javier", age: 27, color: "verde" });
  //await Delete({id:3})
  await update({ age: 30, id: 5 });
  await FinOne({ id: 5 });
  await View({});
}

module.exports = { create, Delete, update, FinOne, View };
